//
//  LoginModuleSegueIdentifiersConstants.h
//  LoginSample
//
//  Created by Vladyslav Bedro on 11/13/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#ifndef LoginModuleSegueIdentifiersConstants_h
#define LoginModuleSegueIdentifiersConstants_h

static NSString* const kLoginToListSegueID = @"ShowListScreenSegue";

#endif /* LoginModuleSegueIdentifiersConstants_h */
