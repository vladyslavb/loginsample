//
//  LoginModuleInteractorOutput.h
//  LoginSample
//
//  Created by Vladyslav Bedro on 13/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

//Frameworks
@import Foundation;

@protocol LoginModuleInteractorOutput <NSObject>

/**
 @author Vladyslav Bedro
 
 Method call router method for transition on listScreen
 */
- (void) didSuccessfulyAuthorize;

@end
