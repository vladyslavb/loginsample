//
//  LoginModuleInteractor.m
//  LoginSample
//
//  Created by Vladyslav Bedro on 13/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

#import "LoginModuleInteractor.h"

// Classes
#import "LoginModuleInteractorOutput.h"

@implementation LoginModuleInteractor


#pragma mark - Methods LoginModuleInteractorInput -

- (void) loginUserToServerWithName: (NSString*) name
                      withPassword: (NSString*) password
{
    /**
     @author Vladyslav Bedro
     
     Stub for study_example with successfuly authorized user
     */
    [self.output didSuccessfulyAuthorize];
}

@end
