//
//  LoginModuleInteractor.h
//  LoginSample
//
//  Created by Vladyslav Bedro on 13/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Classes
#import "LoginModuleInteractorInput.h"

// Protocols
@protocol LoginModuleInteractorOutput;

@interface LoginModuleInteractor : NSObject <LoginModuleInteractorInput>

@property (nonatomic, weak) id<LoginModuleInteractorOutput> output;

@end
