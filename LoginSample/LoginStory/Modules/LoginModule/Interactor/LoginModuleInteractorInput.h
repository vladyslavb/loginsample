//
//  LoginModuleInteractorInput.h
//  LoginSample
//
//  Created by Vladyslav Bedro on 13/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Frameworks
@import Foundation;

@protocol LoginModuleInteractorInput <NSObject>

/**
 @author Vladyslav Bedro
 
 Processing simulation login request to the server
 
 @param name string name parameter
 @param password string password parameter
 */
- (void) loginUserToServerWithName: (NSString*) name
                      withPassword: (NSString*) password;

@end
