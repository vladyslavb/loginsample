//
//  LoginModuleViewController.m
//  LoginSample
//
//  Created by Vladyslav Bedro on 13/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

#import "LoginModuleViewController.h"

// Classes
#import "LoginModuleViewOutput.h"

// Static constants
static const NSUInteger kLoginFieldMinLimitTextLength    = 5;
static const NSUInteger kPasswordFieldMinLimitTextLength = 10;

@interface LoginModuleViewController()

// Outlets
@property (weak, nonatomic) IBOutlet UITextField* loginField;
@property (weak, nonatomic) IBOutlet UITextField* passwordField;

// Methods
- (IBAction) didClickLoginButton: (UIButton*) sender;

@end

@implementation LoginModuleViewController


#pragma mark - Life cycle methods -

- (void) viewDidLoad 
{
	[super viewDidLoad];
    
	[self.output didTriggerViewReadyEvent];
}


#pragma mark - Memory management -

- (void) didReceiveMemoryWarning 
{
    NSLog(@"didReceiveMemoryWarning");

    [super didReceiveMemoryWarning];
}



#pragma mark - Methods LoginModuleViewInput -

- (void) setupInitialState 
{
	/**
	@author Vladyslav Bedro
	
	In this method there is setup of the initial view parameter, 
	which depend from controller life cycle (creation of elements, animation, etc.)
	*/
}


#pragma mark - Actions -

- (IBAction) didClickLoginButton: (UIButton*) sender;
{
    [self.view endEditing: YES];
    
    if([self isValideFieldWithLogin] && [self isValideFieldWithPassword])
    {
        //TODO: check go to the ListVC
        [self.output sendLoginRequestWithName: self.loginField.text
                                 withPassword: self.passwordField.text];
    }
}

//TODO: check
#pragma mark - Validation methods -

- (BOOL) isValideFieldWithLogin
{
    if (self.loginField.text.length >= kLoginFieldMinLimitTextLength)
    {
        return YES;
    }
    
    return NO;
}

- (BOOL) isValideFieldWithPassword
{
    if (self.passwordField.text.length >= kPasswordFieldMinLimitTextLength)
    {
        return YES;
    }
    
    return NO;
}
@end
