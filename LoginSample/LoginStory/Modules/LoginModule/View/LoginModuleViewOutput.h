//
//  LoginModuleViewOutput.h
//  LoginSample
//
//  Created by Vladyslav Bedro on 13/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Frameworks
@import Foundation;

// Protocols
@protocol LoginModuleViewOutput <NSObject>

/**
 @author Vladyslav Bedro
 
 Method which inform presenter, that view is ready for a work
 */
- (void) didTriggerViewReadyEvent;

/**
 @author Vladyslav Bedro
 
 Processing simulation login request to the server through presenter
 
 @param name string name parameter
 @param password string password parameter
 */
- (void) sendLoginRequestWithName: (NSString*) name
                     withPassword: (NSString*) password;

@end
