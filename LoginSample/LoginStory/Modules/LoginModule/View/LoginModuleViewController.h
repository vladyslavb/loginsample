//
//  LoginModuleViewController.h
//  LoginSample
//
//  Created by Vladyslav Bedro on 13/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Frameworks
@import UIKit;

// Classes
#import "LoginModuleViewInput.h"

// Protocols
@protocol LoginModuleViewOutput;

@interface LoginModuleViewController : UIViewController <LoginModuleViewInput>

@property (nonatomic, strong) id<LoginModuleViewOutput> output;

@end
