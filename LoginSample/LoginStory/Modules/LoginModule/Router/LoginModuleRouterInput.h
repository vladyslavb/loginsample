//
//  LoginModuleRouterInput.h
//  LoginSample
//
//  Created by Vladyslav Bedro on 13/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Frameworks
@import Foundation;

@protocol LoginModuleRouterInput <NSObject>

/**
 @author Vladyslav Bedro
 
 Open listScreen
 */
- (void) openListModule;

@end
