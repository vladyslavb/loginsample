//
//  LoginModuleRouter.m
//  LoginSample
//
//  Created by Vladyslav Bedro on 13/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

#import "LoginModuleRouter.h"

// Frameworks
#import <ViperMcFlurry/ViperMcFlurry.h>

// Constants
#import "LoginModuleSegueIdentifiersConstants.h"

@implementation LoginModuleRouter


#pragma mark - Methods LoginModuleRouterInput -

- (void) openListModule
{
    [[self.transitionHandler openModuleUsingSegue: kLoginToListSegueID]
     thenChainUsingBlock: ^id<RamblerViperModuleOutput>(id<RamblerViperModuleInput> moduleInput) {
        
        /**
         @author Vladyslav Bedro
         
         This block we can use in case if need to pass some info to destination controller
         */
        
        return nil;
        
    }];
}

@end
