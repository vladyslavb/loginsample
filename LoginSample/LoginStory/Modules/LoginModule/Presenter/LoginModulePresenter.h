//
//  LoginModulePresenter.h
//  LoginSample
//
//  Created by Vladyslav Bedro on 13/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Classes
#import "LoginModuleViewOutput.h"
#import "LoginModuleInteractorOutput.h"
#import "LoginModuleModuleInput.h"

// Protocols
@protocol LoginModuleViewInput;
@protocol LoginModuleInteractorInput;
@protocol LoginModuleRouterInput;

@interface LoginModulePresenter : NSObject <LoginModuleModuleInput, LoginModuleViewOutput, LoginModuleInteractorOutput>

@property (nonatomic, weak) id<LoginModuleViewInput> view;
@property (nonatomic, strong) id<LoginModuleInteractorInput> interactor;
@property (nonatomic, strong) id<LoginModuleRouterInput> router;

@end
