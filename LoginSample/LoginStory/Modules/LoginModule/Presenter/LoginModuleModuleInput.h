//
//  LoginModuleModuleInput.h
//  LoginSample
//
//  Created by Vladyslav Bedro on 13/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Frameworks
@import Foundation;
#import <ViperMcFlurry/ViperMcFlurry.h>

// Protocols
@protocol LoginModuleModuleInput <RamblerViperModuleInput>

/**
 @author Vladyslav Bedro

 Method initialized start configuration of current module
 */
- (void) configureModule;

@end
