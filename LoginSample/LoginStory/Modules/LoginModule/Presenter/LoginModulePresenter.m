//
//  LoginModulePresenter.m
//  LoginSample
//
//  Created by Vladyslav Bedro on 13/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

#import "LoginModulePresenter.h"

// Classes
#import "LoginModuleViewInput.h"
#import "LoginModuleInteractorInput.h"
#import "LoginModuleRouterInput.h"

@implementation LoginModulePresenter


#pragma mark - Methods LoginModuleModuleInput -

- (void) configureModule 
{
	/**
	@author Vladyslav Bedro
	
	Starting configuration of the module, which dependent to the view state
	*/
}


#pragma mark - Methods LoginModuleViewOutput -

- (void) didTriggerViewReadyEvent 
{
	[self.view setupInitialState];
}

- (void) sendLoginRequestWithName: (NSString*) login
                     withPassword: (NSString*) password
{
    [self.interactor loginUserToServerWithName: login
                                  withPassword: password];
}

#pragma mark - Methods LoginModuleInteractorOutput -

- (void) setupView
{
    
}

- (void) didSuccessfulyAuthorize
{
    [self.router openListModule];
}


@end
