//
//  LoginModuleAssembly.h
//  LoginSample
//
//  Created by Vladyslav Bedro on 13/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Frameworks
#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>

/**
 @author Vladyslav Bedro

 LoginModule module
 */
@interface LoginModuleAssembly : TyphoonAssembly <RamblerInitialAssembly>

@end
