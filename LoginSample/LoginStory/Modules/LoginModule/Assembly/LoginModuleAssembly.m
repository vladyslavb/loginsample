//
//  LoginModuleAssembly.m
//  LoginSample
//
//  Created by Vladyslav Bedro on 13/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

#import "LoginModuleAssembly.h"

// Frameworks
#import <ViperMcFlurry/ViperMcFlurry.h>

// Classes
#import "LoginModuleViewController.h"
#import "LoginModuleInteractor.h"
#import "LoginModulePresenter.h"
#import "LoginModuleRouter.h"


@implementation LoginModuleAssembly


#pragma mark - Initialization methods -

- (LoginModuleViewController*) viewLoginModule 
{
    return [TyphoonDefinition withClass: [LoginModuleViewController class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(output)
                                                    with: [self presenterLoginModule]];
                              [definition injectProperty: @selector(moduleInput)
                                                    with: [self presenterLoginModule]];
                                                    
                          }];
}

- (LoginModuleInteractor*) interactorLoginModule 
{
    return [TyphoonDefinition withClass: [LoginModuleInteractor class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(output)
                                                    with: [self presenterLoginModule]];
                                                    
                          }];
}

- (LoginModulePresenter*) presenterLoginModule
{
    return [TyphoonDefinition withClass: [LoginModulePresenter class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(view)
                                                    with: [self viewLoginModule]];
                              [definition injectProperty: @selector(interactor)
                                                    with: [self interactorLoginModule]];
                              [definition injectProperty: @selector(router)
                                                    with: [self routerLoginModule]];
                                                    
                          }];
}

- (LoginModuleRouter*) routerLoginModule
{
    return [TyphoonDefinition withClass: [LoginModuleRouter class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(transitionHandler)
                                                    with: [self viewLoginModule]];
                                                    
                          }];
}

@end
